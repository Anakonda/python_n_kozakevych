from math import *


def task_1():
    list_threenum = [0, 0, 0]
    for i in range(0, len(list_threenum), 1):
        try:
            list_threenum[i] = int(input('Your num: '))
        except:
            list_threenum[i] = i - 1
    print("Your numbers:", list_threenum)
    print("In range [1,3]:")
    for i in range(0, len(list_threenum), 1):
        if 1 <= list_threenum[i] <= 3:
            print(list_threenum[i])


def task_2():
    year = int(input('Enter a year: '))
    if year % 4 == 0:
        if year % 100 == 0 and year % 400 != 0:
            print("This year has 365 days")
        else:
            print("This year has 366 days")
    else:
        print("This year has 365 days")


def task_3():
    price = float(input('Enter the value: '))
    sum = 0
    if price >= 500:
        if price >= 1000:
            sum = price * 0.95
            print("Your discount is 5% and your sum is ", sum)
        else:
            sum = price * 0.97
            print("Your discount is 3% and your sum is ", sum)
    else:
        sum = price
        print("You get no discount and your sum is ", sum)


def task_4():
    one = float(input('Enter number 1: '))
    two = float(input('Enter number 2: '))
    three = float(input('Enter number 3: '))
    four = float(input('Enter number 4: '))
    answ = 0
    if one < two and one < three and one < four:
        answ = cos(one)
        print('The smallest num is ', one, 'and the cos is ', answ)
    elif two < one and two < three and two < four:
        answ = cos(two)
        print('The smallest num is ', two, 'and the cos is ', answ)
    elif three < one and three < two and three < four:
        answ = cos(three)
        print('The smallest num is ', three, 'and the cos is ', answ)
    elif four < one and four < three and four < two:
        answ = cos(four)
        print('The smallest num is ', four, 'and the cos is ', answ)


def task_5():
    one = float(input('Enter number 1: '))
    two = float(input('Enter number 2: '))
    three = float(input('Enter number 3: '))
    answ = 0
    if one > two and one > three:
        answ = sin(one)
        print('The smallest num is ', one, 'and the sin is ', answ)
    elif two > one and two > three:
        answ = sin(two)
        print('The smallest num is ', two, 'and the sin is ', answ)
    elif three > one and three > two:
        answ = sin(three)
        print('The smallest num is ', three, 'and the sin is ', answ)


def task_6():
    h = float(input('Enter height of triangle: '))
    a = float(input('Enter side of triangle: '))
    s = 0.5 * a * h
    if s % 2 == 0:
        s /= 2
        print('The area split in two is:', s)
    else:
        print('Cant split the area in two')


def task_7():
    list_moth = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                 'October', 'November', 'December']
    mon = int(input('Enter month number:'))
    mon -= 1
    print('The name of month is', list_moth[mon])


def task_8():
    one = float(input('Enter number 1: '))
    two = float(input('Enter number 2: '))
    three = float(input('Enter number 3: '))
    list_num = [one, two, three]
    answ = 0
    for i in range(0, len(list_num), 1):
        if list_num[i] >= 0:
            answ += 1
    print('Amount of positive num is', answ)


def task_9():
    one = int(input('Enter number A: '))
    two = int(input('Enter number B: '))
    answ = 0
    for i in range(one, two + 1, 1):
        answ += i
    print('Sum of a range', answ)


def task_10():
    one = int(input('Enter number A: '))
    two = int(input('Enter number B: '))
    answ = 0
    for i in range(one, two + 1, 1):
        answ += i ** 2
    print('Sum of a range of squares', answ)


def task_11():
    one = int(input('Enter number a: '))
    answ = 0
    for i in range(one, 200 + 1, 1):
        answ += i
    print('Average of num frm', one, 'to 200 is', float(answ / (200 - one + 1)))


def task_12():
    one = int(input('Enter number a: '))
    two = int(input('Enter number b: '))
    i = one
    answ = 0
    while i < two + 1:
        answ += i
        i += 1
    print('Average of num fom', one, 'to', two, 'is', float(answ / (two - one + 1)))


def task_13():
    one = int(input('Enter number a: '))
    answ = 0
    for i in range(one, 50 + 1, 1):
        answ += i ** 2
    print('Sum of a range of squares', answ)


def task_14():
    n = int(input('Enter number n: '))
    k = 0
    while 5 ** k <= n:
        k += 1
    print('K equals', k)


def task_15():
    n = int(input('Enter number n: '))
    for i in range(n):
        if i * i > n:
            print('First num', i * i)
            break


def task_16():
    n = int(input('Enter number n: '))
    p = 4
    i = 1
    while i <= n:
        i += p
        p += 1
    print(i)


task_1(), task_2(), task_3(), task_4(), task_5()
task_6(), task_7(), task_8(), task_9(), task_10()
task_11(), task_12(),task_13(), task_14(),task_15()
task_16()
