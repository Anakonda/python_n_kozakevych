import random
from math import fabs


def task_1():
    n = int(input('Enter length of list: '))
    l_list = []
    for i in range(0, n, 1):
        l_list.append(int(input("your num ")))
    max = -9000
    for i in range(0, n, 1):
        if l_list[i] > max:
            max = l_list[i]
    print("max:", max)
    l_list.reverse()
    print(l_list)


def task_2():
    n = int(input('Enter length of list: '))
    l_list = []
    min = []
    max = []
    for i in range(0, n, 1):
        l_list.append(int(input("your num ")))
    for i in range(0, n, 1):
        if l_list[i] >= 0:
            max.append(l_list[i])
        if l_list[i] < 0:
            min.append(l_list[i])
    print("+: ", max)
    print("-: ", min)


def task_3():
    l_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
    new = []
    sum = 0
    for i in range(0, 20, 1):
        if i % 2 != 0:
            new.append(l_list[i])
            sum += l_list[i]
    print('list', new)
    print('sum:', sum)


def task_4():
    l_list = []
    new = []
    for i in range(0, 30, 1):
        l_list.append(random.randint(-100, 100))
    print('your list', l_list)
    max = -9000
    maxi = 0
    for i in range(0, 30, 1):
        if l_list[i] > max:
            max = l_list[i]
            maxi = i
    print('max', max, 'its place', maxi)
    for i in range(0, 30, 1):
        if l_list[i] % 2 != 0:
            new.append(l_list[i])
    if len(new) == 0:
        print('no pair elem')
    else:
        new.sort()
        new.reverse()
        print(new)


def task_5():
    l_list = []
    for i in range(0, 30, 1):
        l_list.append(random.randint(-100, 100))
    print(l_list)
    for i in range(0, 30, 1):
        if l_list[i] < 0 and i != 30:
            if l_list[i + 1] < 0:
                print(l_list[i], l_list[i + 1])


def task_6():
    l_list = [1, 5, 79, 46, 24, 75, 6, 4, -7, 56]
    print(l_list)
    max = -9000
    for i in range(10):
        if l_list[i] > max:
            max = l_list[i]
    print('max', max)
    new = []
    for i in range(10):
        if l_list[i] != max:
            new.append(l_list[i] * l_list[i])
    new.sort()
    new.reverse()
    print(new)


def task_7():
    l_list = []
    l_round = []
    min = 9000
    new = []
    for i in range(0, 30, 1):
        l_list.append(random.uniform(-100.0, 100.0))
        if abs(l_list[i]) < min:
            min = abs(l_list[i])
    min = round(min, 2)
    for i in l_list:
        i = round(i, 2)
        l_round.append(i)
    l_round.sort()
    print('min for abs', min)
    print('list', l_round)


task_1(), task_2(), task_3(), task_4(), task_5(),
task_6(), task_7()
