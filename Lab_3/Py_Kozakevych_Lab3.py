def task_1():
    S = input('Enter a couple of words ')
    a = input('Enter your latter ')
    words = S.split(" ")
    l = [word[0] for word in words]
    count = 0
    for i in range(0, len(l), 1):
        if (l[i] == a.lower()):
            count += 1
    print(l)
    print("first letters that match", count)


def task_2():
    S = input('Enter something with ":" ')
    dot = 0
    for i in range(0, len(S), 1):
        if (S[i] == ":"):
            dot += 1
    new = S.replace(":", "%")
    print(new)
    print("changes", dot)


def task_3():
    S = input('Enter something with "." ')
    dot = 0
    for i in range(0, len(S), 1):
        if (S[i] == "."):
            dot += 1
    new = S.replace(".", "")
    print(new)
    print("changes", dot)


def task_4():
    S = input('Enter a couple of words ')
    count = len(S)
    a = 0
    for i in range(0, len(S), 1):
        if (S[i] == "a"):
            a += 1
    new = S.replace("a", "o")
    print(new)
    print("changes", a)
    print("amount of symbols", count)


def task_5():
    S = input('Enter a couple of words ')
    print(S.lower())


def task_6():
    S = input('Enter a couple of words ')
    o = 0
    for i in range(0, len(S), 1):
        if (S[i] == "o"):
            o += 1
    new = S.replace("o", "")
    print(new)
    print("changes", o)


def task_7():
    S = input('Enter a couple of words ')
    S1 = S[:int((len(S) + 1) / 2)]
    print(S.replace('п', '*', len(S1) - len(S1.replace('п', ''))))


def task_8():
    S = input('Enter a couple of words ')
    w = input('Enter your word ')
    words = S.split(" ")
    count = 0
    for i in range(0, len(words), 1):
        if (w == words[i]):
            count += 1
    print("amount of words", count)


def task_9():
    S = input('Enter a couple of words ')
    print(S.title())


def task_10():
    S = input('Enter a couple of words ')
    n = input('Enter your latter N ')
    p = input('Enter your latter P ')
    words = S.split(" ")
    for i in range(0, len(words), 1):
        if (words[i].startswith(n) and words[i].endswith(p)):
            print(words[i])


def task_11():
    S = input('Enter a couple of words ')
    o = 0
    for i in range(0, len(S), 1):
        if (S[i].lower() == "o" or S[i].lower() == "a" or
                S[i].lower() == "i" or S[i].lower() == "y" or S[i].lower() == "u" or S[i].lower() == "e"):
            o += 1
    print("amount", o)


def task_12():
    S = input('Enter a couple of words ')
    o = 0
    for i in range(0, len(S), 1):
        if (S[i].lower() != "o" and S[i].lower() != "a" and
                S[i].lower() != "i" and S[i].lower() != "y" and S[i].lower() != "u"
                and S[i].lower() != "e" and S[i].lower() != " "):
            o += 1
    print("amount", o)


def task_13():
    S = input('Enter a couple of words ')
    words = S.split(" ")
    l = ""
    w = [word[0] for word in words]
    for i in range(0, len(words), 1):
        if (w[i].isupper()):
            l += words[i]
            l += " "
    print("with capital first:", l)


task_1(), task_2(), task_3(), task_4(), task_5()
task_6(), task_7(), task_8(), task_9(), task_10()
task_11(), task_12(), task_13()
